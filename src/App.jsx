import logo from "./logo.svg";
import "./App.scss";

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.jsx</code> and save to reload.
        </p>
        <h2>Training ReactJS</h2>
      </header>
    </div>
  );
};

export default App;
